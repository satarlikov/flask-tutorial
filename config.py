import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'secretp@ssw0rd'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'postgresql+psycopg2://postgres:dctyfakjn@localhost/microblog'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
